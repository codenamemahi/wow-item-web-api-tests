# test.py
# contains all the test cases
import unittest

from http_get import *

class Test_Bad_Set_Requests(unittest.TestCase):
    ''' test case to verify the correct response is received when making a bad request'''
    def setUp(self):
        self.bad_request = Bad_Request()
        self.setId = 1060
    def test_invalid_header(self):
        '''verify the correct response is received when a request is made with a bad header'''
        bad_header_test_result = self.bad_request.badheader_get_set(self.setId)
        self.assertTrue(bad_header_test_result)
        
    def test_invalid_signature(self):
        '''verify the correct response is received when a request is made with an invalid signature'''
        bad_signature_test_result = self.bad_request.badsig_get_set(self.setId)
        self.assertTrue(bad_signature_test_result)
    
    def test_not_found(self):
        '''verify the correct response is received when a request is made for a non-existent resource'''
        not_found_test_result = self.bad_request.not_found_get_set('invalidId')
        self.assertTrue(not_found_test_result)

class Test_Item_Set_Types(unittest.TestCase):
    ''' test case to verify the keys of an item set have values of the correct type'''
    def setUp(self):
        self.get_item = Get_Item()
        self.setId = 1060
        self.itemSet = self.get_item.set_by_id(self.setId)
    
    def test_id_type(self):
        '''verify id is type int'''
        self.assertTrue(type(self.itemSet['id'])==int)
    
    def test_items_type(self):
        '''verify items is type list'''
        self.assertTrue(type(self.itemSet['items'])==list)
    
    def test_items_subtype(self):
        '''verify the contents in the list items is type int'''
        for contents in self.itemSet['items']:
            self.assertTrue(type(contents)==int)

    def test_name_type(self):
        '''verify name is type str'''
        self.assertTrue(type(self.itemSet['name'])==str)
    
    def test_setBonuses_type(self):
        '''verify setBonuses is type list'''
        self.assertTrue(type(self.itemSet['setBonuses'])==list)
    
    def test_setBonuses_subtype(self):
        '''verify the contents of the list setBonuses is type dict'''
        for contents in self.itemSet['setBonuses']:
            self.assertTrue(type(contents)==dict)

class Test_History_Text_Comparison(unittest.TestCase):
    '''test case to do a historical text comparison, will fail if there have been any changes'''
    def setUp(self):
        self.known_item = open('known/18803.json')        
        self.knownItemJson = json.loads(self.known_item.read())
        self.known_set = open('known/1060.json')
        self.knownSetJson = json.loads(self.known_set.read())
        self.get_item = Get_Item()
        
    def tearDown(self):
        self.known_item.close()
        self.known_set.close()
        
    def test_text_item_history(self):
        '''historical text comparison for known item'''
        unknown = self.get_item.get_item(18803)
        self.assertEqual(self.knownItemJson, unknown)
        
    def test_text_set_history(self):
        '''historical text comparison for known item set'''
        unknown= self.get_item.set_by_id(1060)
        self.assertEqual(self.knownSetJson, unknown)

class Test_History_Type_Comparison(unittest.TestCase):
    '''test case to do a historical comparison of data type for the JSON values'''
    def setUp(self):
        self.known_item = open('known/18803.json')
        self.knownItemJson= json.loads(self.known_item.read())
        self.known_set = open('known/1060.json')
        self.knownItemSetJson= json.loads(self.known_set.read())
        self.get_item = Get_Item()

    def tearDown(self):
        self.known_item.close()
        self.known_set.close()

    def test_type_item_history(self):
        '''historical type comparison for known item'''
        unknown = self.get_item.get_item(18803)
        for key in unknown.keys():
            self.assertTrue(type(unknown[key])==type(self.knownItemJson[key]))
            
    def test_type_set_history(self):
        '''historical type comparison for known item set'''
        unknown = self.get_item.set_by_id(1060)
        for key in unknown.keys():
            self.assertTrue(type(unknown[key])==type(self.knownItemSetJson[key]))

        
class Test_Set_Continuity(unittest.TestCase):
    '''testing that the information in a set is continuous'''
    def setUp(self):
        self.get_item = Get_Item()

    def test_set_items(self):
        '''verify that for all the items in a set, that those items have matching set item lists'''
        set= self.get_item.set_by_id(1060)
        setItems = set['items']
        for items in setItems:
            subItem = self.get_item.get_item(items)
            subItemList = subItem['itemSet']['items']
            self.assertEqual(setItems, subItemList)
    def test_set_bonuses(self):
        '''verify that for all the set bonuses in a set, that all items in the set have the same set bonuses'''
        set = self.get_item.set_by_id(1060)
        setItems = set['items']
        setBonuses = set['setBonuses']
        for items in setItems:
            subSetBonuses = self.get_item.get_item(items)['itemSet']['setBonuses']
            self.assertEqual(setBonuses, subSetBonuses)

if __name__ == '__main__':
    unittest.main(verbosity=2)
        
