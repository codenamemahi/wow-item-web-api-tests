import httplib2
import json
import time

http = httplib2.Http()

class Get_Item:
    '''A class for getting item or item set data'''
    def __init__(self):
        '''Class constructor
        
        Args:
            None
        '''
        # if other region to be tested change it here
        self.baseUrl = 'http://us.battle.net/api/wow/item/'

    def set_by_id(self, setId: int) -> json:
        '''requests item set information from the server being tested.
        
        Args:
            setId(int): the set ID for the item set being requested
        
        Returns:
            A JSON object with properties for the requested setID
            '''
        url = self.baseUrl+'set/'+str(setId)
        resp, content = http.request(url)
        content = json.loads(content.decode('utf-8'))
    
        return content

    def get_item(self, itemId: int) -> json:
        '''requests individual item information from the server being tested.
        
        Args:
            itemId(int): the item ID for the item  being requested
        
        Returns:
            A JSON object with properties for the requested itemId
            '''
        url = self.baseUrl+str(itemId)
        resp, content = http.request(url)
        content = json.loads(content.decode('utf-8'))
    
        return content

class Bad_Request:
    '''A class for testing server response for bad requests'''
    def __init__(self):
        '''class constructor
        
        Args:
            None
        '''
        # if other region to be tested change it here
        self.baseUrl = 'http://us.battle.net/api/wow/item/'
    
    def badheader_get_set(self, setId: int) -> bool:
        '''test function to pass an invalid header
        
            Args:
                setId(int): the set ID for the item set being requested (with bad header)
                
            Returns:
                True, if the correct error response is recieved, False otherwise
        '''
        url = self.baseUrl+'set/'+str(setId)
        resp, content = http.request(url, headers = {'Authorization: BNET':'blahblah blah'} )
        content = json.loads(content.decode('utf-8'))
    
        return content['reason']=='Invalid authentication header.'

    def badsig_get_set(self, setId: int) -> bool:
        '''test function to pass invalid credentials and verify the correct error response is received
                    Args:
                setId(int): the set ID for the item set being requested (with bad signature)
                
            Returns:
                True, if the correct error response is received, False otherwise
        '''
        now = time.gmtime()
        url = self.baseUrl+str(setId)
        header = {'Date' : time.strftime( '%a, %d, %b, %Y, %H:%m:%S GMT', now),
                  'Authorization' : 'BNET c1fbf21b79c03191d:fakesignature'
                  }
        resp, content = http.request(url, headers=header)
        content = json.loads(content.decode('utf-8'))
    
        return content['reason']=='Invalid Application'

    def not_found_get_set(self, setId: int) -> bool:
        '''test function to request a page that does not exist and verify the correct error response is received
                    Args:
                setId(int): the set ID for the item set being requested (use the incorrect type for this test)
                
            Returns:
                True, if the correct error response is received, False otherwise
        '''            
        url = self.baseUrl+'set/'+str(setId)
        resp, content = http.request(url)
        content = json.loads(content.decode('utf-8'))
        
        return content['reason']=='When in doubt, blow it up. (page not found)'
